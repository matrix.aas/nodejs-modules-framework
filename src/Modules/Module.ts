import Logger from "../Components/Logger";
import {App, AppInstance} from "../App";
import {ListenerFn, OnOptions} from "eventemitter2";

export interface ModuleInformation {
    /**
     * Module name
     */
    name: string,

    /**
     * Modules after which your module will load
     */
    dependencies?: string[] | null,

    /**
     * Name in console (logger) - if it is empty (null) - equals module name
     */
    loggerName?: string | null,

    /**
     * Console commands implemented by your module
     * (Only these commands will be passed to the handleConsoleCommand)
     */
    consoleCommands?: { [key: string]: string } | null,

    /**
     * The module must be loaded, if the "allowLoad" function prohibits loading, then the application will terminate abnormally
     */
    required?: boolean,
}

export type ModuleEvent = string;

/**
 * All module must extends this!
 *
 * *** Functions who marked with the "Quick Access", accelerators access the same functionality within the application.
 */
export abstract class Module {
    /**
     * Information about current module after it initialized in ModuleManager
     */
    public information: ModuleInformation = null;

    /**
     * Current module logger
     */
    protected logger: Logger = null;

    /**
     * Current module provided states
     */
    private _states: Map<string, () => any> = new Map();

    /**
     * Is module initialized
     */
    private _initialized = false;

    /**
     * Is module runned
     */
    private _runned = false;

    /**
     * -- REQUIRED FIELD ON ANY MODULE --
     * Information about this module for ModuleManager
     */
    public static get information(): ModuleInformation {
        return null;
    }

    /**
     * (Quick Access)
     * Global app instance
     */
    protected get app(): App {
        return AppInstance;
    }

    /**
     * Checking whether the module can be loaded.
     * It is executed before the module is initialized, right after its instantiation.
     */
    public async allowLoad(): Promise<boolean> {
        return true;
    }

    /**
     * Init current module (called from ModuleManager)
     */
    public async init(): Promise<void> {
        if (this._runned) {
            throw new Error('Module runned before initialized, very strange!');
        }
        if (this._initialized) {
            throw new Error('Module already initialized!');
        }

        await this.registerEventHandlers();
        await this.provideStates();

        this._initialized = true;
    }

    /**
     * Run current module (called from app main thread)
     */
    public async run(): Promise<void> {
        if (!this._initialized) {
            throw new Error('Module not initialized!');
        }
        if (this._runned) {
            throw new Error('Module already runned!');
        }
        this._runned = true;
    }

    /**
     * Handle console command
     */
    public async handleConsoleCommand(command: string, ...args: string[]): Promise<boolean> {
        return false;
    }

    /**
     * (Quick Access)
     * Get module from ModuleManager by it name
     */
    protected module(name: string): Module | null {
        return AppInstance.module(name);
    }

    /**
     * (Quick Access)
     * Emit event to event bus
     */
    public emit(event: ModuleEvent, owner: Module, recipientModuleName: string, ...args: any[]): boolean {
        return AppInstance.emit(event, owner, recipientModuleName, ...args);
    }

    /**
     * (Quick Access)
     * Emit event to event bus async
     */
    public emitAsync(event: ModuleEvent, owner: Module, recipientModuleName: string, ...args: any[]): Promise<any[]> {
        return AppInstance.emitAsync(event, owner, recipientModuleName, ...args);
    }

    /**
     * Emit event to event bus to "module" from self
     */
    public emitFromSelf(event: ModuleEvent, recipientModuleName: string, ...args: any[]): boolean {
        return this.emit(event, this, recipientModuleName, ...args);
    }

    /**
     * Emit event to event bus to "module" from self
     */
    public emitFrom(event: ModuleEvent, owner: Module, ...args: any[]): boolean {
        return this.emit(event, owner, this.information.name, ...args);
    }

    /**
     * Emit event to event bus to self from self
     */
    public emitLocal(event: ModuleEvent, ...args: any[]): boolean {
        return this.emit(event, this, this.information.name, ...args);
    }

    /**
     * (Quick Access)
     * Subscribe to event in event bus
     */
    public subscribe(event: ModuleEvent, moduleName: string, handler: ListenerFn, options?: boolean | OnOptions): void {
        AppInstance.subscribe(event, moduleName, handler, options);
    }

    /**
     * Subscribe to self module event in event bus
     */
    public subscribeSelf(event: ModuleEvent, handler: ListenerFn, options?: boolean | OnOptions): void {
        this.subscribe(event, this.information.name, handler, options);
    }

    /**
     * Redefine this in your module for register event handlers
     */
    protected async registerEventHandlers(): Promise<void> {
        // Nothing...
    }

    /**
     * (Quick Access)
     * Get data from global app config
     */
    protected config(key: string = null, def: any = null): any {
        if (!key) {
            return this.app.config();
        }
        return this.app.config().get(key, def);
    }

    /**
     * Request current module state
     */
    public requestSelfState(key: string, def: any = undefined): any {
        if (this._states.has(key = key.toLowerCase().trim())) {
            return this._states.get(key)();
        }
        return def;
    }

    /**
     * (Quick Access)
     * Request other module state
     */
    protected requestState(moduleName: string, key: string, def: any = undefined): any {
        return AppInstance.getModuleManager().requestState(moduleName, key, def);
    }

    /**
     * Provide module state
     */
    protected provideState(key: string, handler: () => any): void {
        this._states.set(key.toLowerCase().trim(), handler);
    }

    /**
     * Redefine this in your module for provide states
     */
    protected async provideStates(): Promise<void> {
        // Nothing...
    }
}
