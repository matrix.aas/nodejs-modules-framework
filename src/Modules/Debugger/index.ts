import {Module, ModuleInformation} from "../Module";

export default class Debugger extends Module {
    public static get information(): ModuleInformation {
        return {
            name: "Debugger",
            consoleCommands: {
                'fake-event': 'Send fake-event to event bus',
                'request-module-data': 'Request module data',
            },
        };
    }

    async allowLoad(): Promise<boolean> {
        return process.env.NODE_ENV === 'development';
    }

    protected async registerEventHandlers(): Promise<void> {
        await super.registerEventHandlers();

        this.subscribeSelf('hello-world', () => {
            this.logger.info('Hello world!');
        });
    }

    async handleConsoleCommand(command: string, ...args): Promise<boolean> {
        if (command === 'fake-event') {
            if (args.length < 2) {
                this.logger.info('Use: fake-event "module-name" "event-name" arguments');
                return true;
            }

            this.logger.info(`Event "${args[1]}" to module "${args[0]}" successfully emited!`);

            const _args = args.slice(2).map(arg => {
                if (arg === 'true') {
                    return true;
                } else if (arg === 'false') {
                    return false;
                } else if (!isNaN(arg)) {
                    return parseFloat(arg);
                }
                return arg;
            });

            try {
                this.emitFromSelf(args[1], args[0], ..._args);
            } catch (e) {
                this.logger.warn('Exception in emit!', e);
                return true;
            }
            return true;
        }

        if (command === 'request-module-data') {
            if (args.length < 2) {
                this.logger.info('Use: request-module-data "module-name" "key"');
                return true;
            }

            try {
                let value = this.requestState(args[0], args[1]);

                if (value === undefined) {
                    this.logger.warn(`Provided key "${args[1]}" in module "${args[0]}" not exists!`);
                    return true;
                }

                if (value instanceof Promise) {
                    this.logger.info('Provided value is Promise! Waiting...');

                    value = await value;
                }

                this.logger.info('Provided value:', value);
            } catch (e) {
                this.logger.warn(`Module "${args[0]}" not found! Or something went wrong!`);
            }

            return true;
        }

        return false;
    }
}
