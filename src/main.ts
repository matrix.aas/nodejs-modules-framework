import {AppInstance} from "./App";
import colors from 'colors/safe';

(async () => {
    if (process.env.NODE_ENV === 'development') {
        process.stdout.write(colors.red("Running in development mode!\n"));
    }

    try {
        await AppInstance.init();
        await AppInstance.run();
    } catch (e) {
        if (process.env.NODE_ENV === 'development') {
            process.stdout.write(colors.red('Global error!\n'));
            process.stdout.write(`${e.toString()}\n`);
        }
        process.exit(1);
    }
})();
