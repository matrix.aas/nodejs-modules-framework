class ArgsParser {
    protected _plains: string[] = [];
    protected _flags: string[] = [];
    protected _properties: Map<string, string | number> = new Map();

    constructor() {
        process.argv.slice(2).forEach((arg, index) => {
            const argInfo = arg.split('=');

            let argName = argInfo[0];

            const dashPosition = argName.indexOf('-');
            if (dashPosition === 0) {
                argName = argName.slice(argName.slice(0, 2).lastIndexOf('-') + 1);
            } else if (dashPosition === -1) {
                this._plains.push(argName);
                return;
            }

            if (argInfo.length === 2) {
                const argValue: any = argInfo[1];
                if (!isNaN(argValue)) {
                    this._properties.set(argName, +argValue);
                } else {
                    this._properties.set(argName, argValue);
                }
            } else {
                this._flags.push(argName);
            }
        });
    }

    public plains(): string[] {
        return this._plains;
    }

    public plain(index: number): string | undefined {
        return index < this._plains.length ? this._plains[index] : undefined;
    }

    public hasFlag(key: string): boolean {
        return this._flags.indexOf(key) > -1;
    }

    public flags(): string[] {
        return this._flags;
    }

    public hasProperty(key: string): boolean {
        return this._properties.has(key);
    }

    public getProperty(key: string, def: string | number | undefined = undefined): string | number | undefined {
        return this.hasProperty(key) ? this._properties.get(key) : def;
    }

    public properties(): Map<string, string | number> {
        return this._properties;
    }
}

export default new ArgsParser();
