export function cloneObject<T>(source: T, deep = false): T {
    const result: any = {};
    for (const key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
            if (deep && source[key] instanceof Object) {
                if (source[key] instanceof Array) {
                    result[key] = [];
                    (source[key] as any).forEach(item => {
                        if (item instanceof Object) {
                            result[key].push(cloneObject(item, true));
                        } else {
                            result[key].push(item);
                        }
                    });
                } else {
                    result[key] = cloneObject(source[key]);
                }
            } else {
                result[key] = source[key];
            }
        }
    }

    return result;
}
