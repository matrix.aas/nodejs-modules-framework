export function sizeToHumanReadable(bytes: number, fixed = 1): string {
    bytes = Math.abs(bytes);

    const units = ['b', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb'];
    let loop = 0;

    // calculate
    while (bytes >= 1024) {
        bytes /= 1024;
        ++loop;
    }
    return `${bytes.toFixed(fixed)} ${units[loop]}`;
}
