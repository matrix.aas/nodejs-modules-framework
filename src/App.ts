import path from 'path';
import readline from 'readline';
import fs from "fs";
import moment from 'moment';

import Logger, {addLoggerDefaultOutputStream} from "./Components/Logger";
import ModuleManager from "./Components/ModuleManager";
import {EventEmitter2, ListenerFn, OnOptions} from "eventemitter2";
import {Module, ModuleEvent} from "./Modules/Module";
import {Config} from "./Components/Config";
import ArgsParser from "./Services/ArgsParser";

const logger = new Logger('Application');

export class App {
    protected console: readline.Interface = null;
    protected _config: Config = null;
    protected moduleManager = new ModuleManager();

    protected eventBus = new EventEmitter2({
        wildcard: true,
        delimiter: ':',
        newListener: true,
        maxListeners: 20,
        verboseMemoryLeak: false
    });

    /**
     * Initialize application
     */
    public async init(): Promise<void> {
        logger.info('Initializing application...');

        await this.loadConfig();

        let logFilePath = this._config.get('paths.logger', false);
        if (logFilePath && typeof logFilePath === 'string') {
            logFilePath = path.join(logFilePath, `${moment().format('YYYY-MM-DD_HH-mm-ss')}.log`);

            logger.info(`Starting log to file "${logFilePath}"...`);

            const dir = path.dirname(logFilePath);
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir, {recursive: true});
            }

            await new Promise(resolve => {
                const outputStream = fs.createWriteStream(logFilePath);
                outputStream.on('error', e => {
                    throw new Error(`Can't write to log file "${logFilePath}"! ${e.message}`);
                });
                addLoggerDefaultOutputStream(outputStream);
                logger.info(`Log to file started!`);
                process.nextTick(() => {
                    resolve(null);
                });
            });
        }

        logger.info(`Version ${process.env.__VERSION__}, build #${process.env.__BUILD_NUMBER__} from ${process.env.__BUILD_DATE__}`);

        await this.moduleManager.loadStaticLinkedModules();
    }

    /**
     * Run application
     */
    public async run(): Promise<void> {
        logger.info('Running...');

        // App works...

        logger.info(`Fully loaded and started to work! Enjoy ;)`);

        await this.moduleManager.doRun();

        this.initializeConsole();
    }

    /**
     * Get ModuleManager
     */
    public getModuleManager(): ModuleManager {
        return this.moduleManager;
    }

    /**
     * Get module from ModuleManager by it name
     */
    public module(name: string): Module | null {
        return this.moduleManager.module(name);
    }

    /**
     * Get current configuration
     */
    public config(): Config {
        return this._config;
    }

    /**
     * ! IMPORTANT ! Please, use this function instead getEventBus().emit() !!!!
     * Emit event to event bus
     */
    public emit(event: ModuleEvent, owner: Module, recipientModuleName: string, ...args: any[]): boolean {
        return this.eventBus.emit(`${recipientModuleName.trim().toLowerCase()}:${event}`, owner, ...args);
    } //module.information.name.toLowerCase()

    /**
     * ! IMPORTANT ! Please, use this function instead getEventBus().emitAsync() !!!!
     * Emit event to event bus async
     */
    public emitAsync(event: ModuleEvent, owner: Module, recipientModuleName: string, ...args: any[]): Promise<any[]> {
        return this.eventBus.emitAsync(`${recipientModuleName.trim().toLowerCase()}:${event}`, owner, ...args);
    }

    /**
     * Subscribe to event in event bus
     */
    public subscribe(event: ModuleEvent, moduleName: string, handler: ListenerFn, options?: boolean | OnOptions): void {
        this.eventBus.on(`${moduleName.trim().toLowerCase()}:${event}`, handler, options);
    }

    protected initializeConsole(): void {
        this.console = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        const googby = () => {
            logger.simple('Goodbye!');
            process.exit(0);
        };

        this.console.on('close', googby);
        process.on('SIGINT', googby);

        this.console.on('line', line => {
            let buf = '';
            let args: string[] = [];
            let inQuotes = false;
            for (const char of line) {
                if (char === '\"') {
                    if (inQuotes) {
                        args.push(buf);
                        buf = "";
                        inQuotes = false;
                    } else {
                        inQuotes = true;
                    }
                } else if (char === ' ' && !inQuotes) {
                    if (buf.trim().length) {
                        args.push(buf.trim());
                        buf = '';
                    }
                } else {
                    buf += char;
                }
            }
            if (buf.trim().length) {
                args.push(buf.trim());
            }

            if (args.length) {
                this.handleConsoleCommand(args[0].toLowerCase(), ...args.slice(1)).finally(() => {
                    process.stdout.write('\r> ');
                });
            } else {
                process.stdout.write('\r> ');
            }
        });
    }

    protected async handleConsoleCommand(command: string, ...args: string[]): Promise<void> {
        if (command === 'exit' || command === 'stop') {
            process.emit('SIGINT', 'SIGINT');
            return;
        }

        if (command === 'modules') {
            const loadedModulesNames = this.moduleManager.getModulesNames();
            logger.info(`Loaded ${loadedModulesNames.length} modules:`);
            for (const name of loadedModulesNames) {
                logger.info(`-- ${name}`);
            }
            return;
        }

        if (command === 'version') {
            logger.info(`${process.env.__VERSION__}, build #${process.env.__BUILD_NUMBER__} from ${process.env.__BUILD_DATE__}`,);
            return;
        }

        if (command === 'config-get') {
            if (args.length !== 1) {
                logger.info('Use: config-get "key"');
            } else {
                logger.info(`Config value of key "${args[0]}" is:`, this.config().get(args[0]));
            }
            return;
        }

        if (command === 'help') {
            const availableCommands = this.moduleManager.getConsoleCommands();
            logger.info(`Available commands:`);

            logger.info(`-- GLOBAL`);
            logger.info(`----> version`);
            logger.info(`----> modules`);
            logger.info(`----> help`);
            logger.info(`----> config-get`);
            logger.info(`----> exit / stop`);

            for (const [moduleName, commands] of Object.entries(availableCommands)) {
                const commandsArray = Object.entries(commands);
                if (commandsArray.length) {
                    logger.info(`-- ${moduleName}`);
                    commandsArray.forEach(_command => logger.info(`----> ${_command[0]} - ${_command[1]}`));
                }
            }
            return;
        }

        try {
            if (!await this.moduleManager.handleConsoleCommand(command, ...args)) {
                logger.info('Command not found!');
            }
        } catch (e) {
            logger.err('Exception in command! ', e);
        }
    }

    protected async loadConfig(): Promise<void> {
        const configPath: string = (
            ArgsParser.getProperty('config') ||
            ArgsParser.getProperty('c') ||
            path.join(process.cwd(), 'config.json')
        ) as string;

        logger.info(`Loading config from "${configPath}"...`);

        this._config = new Config(configPath);
        this._config.addPercentVariable('cwd', process.cwd());

        try {
            await this._config.load();
        } catch (e) {
            logger.error(e.message || 'Can\'t load config!');
            throw e;
        }

        logger.info('Config loaded!');
    }
}

export const AppInstance: App = new App();
