import {promises as fs} from "fs";

export class Config {
    protected filePath: string;
    protected percentVariables: Map<string, string> = new Map();
    protected originalConfig: object = null;
    protected config: object = null;

    constructor(filePath: string) {
        this.filePath = filePath;
    }

    public async load(): Promise<void> {
        this.originalConfig = null;
        this.config = null;

        try {
            await fs.lstat(this.filePath);
        } catch (e) {
            throw new Error(`Config "${this.filePath}" not found!`);
        }

        try {
            const content = (await fs.readFile(this.filePath)).toString();
            this.originalConfig = JSON.parse(content);
            this.config = JSON.parse(content);
        } catch (e) {
            throw new Error(`Can't read config "${this.filePath}"!`);
        }

        const doVarReplace = (obj) => {
            for (const key in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, key) && !Array.isArray(obj[key])) {
                    if (typeof obj[key] === 'object') {
                        doVarReplace(obj[key]);
                    } else if (typeof obj[key] === 'string') {
                        obj[key] = obj[key].replace(/%(\w+?)%/g, (match, varName): string => {
                            varName = varName.toLowerCase();
                            if (this.percentVariables.has(varName)) {
                                return this.percentVariables.get(varName);
                            }
                            return match;
                        });
                    }
                }
            }
        }
        doVarReplace(this.config);
    }

    public addPercentVariable(name: string, value: string): void {
        this.percentVariables.set(name.trim().toLowerCase(), value);
    }

    public has(key: string | null): boolean {
        if (!this.config) {
            return false;
        }

        if (!key) {
            return true;
        }

        const keys = key.split('.');
        let config = this.config;
        while (key = keys.shift()) {
            if (!(key in config)) {
                return false;
            }

            const value = config[key];
            if (keys.length) {
                if (typeof value !== 'object' || Array.isArray(value)) {
                    return false;
                }
                config = value;
            } else {
                return true;
            }
        }
        return false;
    }

    public get(key: string | null, def: any = null): any {
        if (!this.config) {
            return def;
        }

        if (!key) {
            return this.config;
        }

        const keys = key.split('.');
        let config = this.config;
        while (key = keys.shift()) {
            if (!(key in config)) {
                break;
            }

            const value = config[key];
            if (keys.length) {
                if (typeof value !== 'object' || Array.isArray(value)) {
                    return def;
                }
                config = value;
            } else {
                return value;
            }
        }
        return def;
    }

    public plain(key?: string | null): Map<string, string> | null {
        if (!this.config) {
            return null;
        }

        if (!key) {
            return this._plain(null, this.config);
        }

        const rawKey = key;
        const keys = key.split('.');
        let config = this.config;
        while (key = keys.shift()) {
            if (!(key in config)) {
                return null;
            }

            const value = config[key];

            if (typeof value !== 'object' || Array.isArray(value)) {
                return null;
            }

            if (keys.length) {
                config = value;
            } else {
                return this._plain(rawKey, value);
            }
        }

        return null;
    }

    public edit(): ConfigEditor {
        return new ConfigEditor(this.filePath, this.originalConfig, this.config);
    }

    public path(): string {
        return this.filePath;
    }

    protected _plain(key: string | null, obj: object): Map<string, string> {
        key = key ? `${key}.` : '';
        const result = new Map<string, string>();
        for (const oKey in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, oKey)) {
                if (typeof obj[oKey] === 'object' && !Array.isArray(obj[oKey])) {
                    const res = this._plain(`${key}${oKey}`, obj[oKey]);
                    for (const [mKey, value] of res) {
                        result.set(mKey, value);
                    }
                } else {
                    result.set(`${key}${oKey}`, obj[oKey]);
                }
            }
        }
        return result;
    }
}

class ConfigEditor {
    constructor(
        protected filePath: string,
        protected originalConfig: object = null,
        protected config: object = null
    ) {
    }

    public set(key: string, value: any): this {
        if (!this.config || !this.originalConfig) {
            return this;
        }
        const keys = key.split('.');
        let configA = this.originalConfig;
        let configB = this.config;
        while (key = keys.shift()) {
            if (!Object.prototype.hasOwnProperty.call(configA, key)) {
                configA[key] = {};
            }

            if (!Object.prototype.hasOwnProperty.call(configB, key)) {
                configB[key] = {};
            }

            if (keys.length) {
                if (typeof configA[key] !== 'object') {
                    configA[key] = {};
                }
                if (typeof configB[key] !== 'object') {
                    configB[key] = {};
                }
                configA = configA[key];
                configB = configB[key];
            } else {
                configA[key] = value;
                configB[key] = value;
            }
        }

        return this;
    }

    public delete(key: string): this {
        if (!this.config || !this.originalConfig) {
            return this;
        }
        const keys = key.split('.');
        let configA = this.originalConfig;
        let configB = this.config;
        while (key = keys.shift()) {
            if (!Object.prototype.hasOwnProperty.call(configA, key) || !Object.prototype.hasOwnProperty.call(configB, key)) {
                return this;
            }

            if (keys.length) {
                if (typeof configA[key] !== 'object' || typeof configB[key] !== 'object') {
                    return this;
                }
                configA = configA[key];
                configB = configB[key];
            } else {
                delete configA[key];
                delete configB[key];
            }
        }

        return this;
    }

    public async commit(): Promise<boolean> {
        try {
            await fs.writeFile(this.filePath, JSON.stringify(this.originalConfig, null, 4));
        } catch (e) {
            return false;
        }
        return true;
    }
}
