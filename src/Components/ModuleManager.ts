import Logger from "./Logger";
import {Module} from "../Modules/Module";

import StaticLinkedModules from '../Modules';

const logger = new Logger('Module-Manager');

class AlreadyRegisteredError extends Error {
    constructor(name: string) {
        super(`Module with same name "${name}" already registered!`);
    }
}

class UnresolvedDependencyError extends Error {
    constructor() {
        super('Unresolved module dependency!');
    }
}

class ModuleInitializationError extends Error {
    private e: Error = null;

    constructor(name: string, e: Error) {
        super(`Can't initialize module "${name}"!`);
        this.e = e;
    }

    public error(): Error {
        return this.e;
    }
}

interface AvailableCommands {
    [key: string]: { [key: string]: string }
}

export default class ModuleManager {
    protected modules = new Map<string, Module>();
    protected modulesCommands = new Map<string, Module[]>();

    public async loadStaticLinkedModules(): Promise<void> {
        logger.info('Loading static linked modules...');

        const modules = new Map<string, Module>();

        if (!StaticLinkedModules || typeof StaticLinkedModules !== 'object') {
            logger.err('Static linked modules structure is broken!');
            throw new Error('Static linked modules structure is broken!');
        }

        const requiredModules: Array<string[]> = [];

        for (const moduleObjName in StaticLinkedModules) {
            if (!Object.prototype.hasOwnProperty.call(StaticLinkedModules, moduleObjName)) {
                continue;
            }

            const module: Module = StaticLinkedModules[moduleObjName];

            if (!module.information ||
                typeof module.information !== 'object' ||
                !module.information.name) {
                logger.err(`Can't load module "${moduleObjName}", it must be extends from Module and have 'information' static field!`);
                continue;
            }

            const moduleInformation = module.information;
            const moduleName = moduleInformation.name.toLowerCase();

            if (moduleInformation.required) {
                requiredModules.push([moduleName, moduleInformation.name, moduleObjName]);
            }

            if (!moduleName.match(/^[0-9a-z_-]+$/i)) {
                logger.err(`Can't load module "${moduleObjName}", name must be match with pattern "^[0-9a-zA-Z_-]+$"!`);
                continue;
            }

            /** Skip module if it already registered */
            if (modules.has(moduleName)) {
                logger.err(`Can't load module "${moduleInformation.name}" from "${moduleObjName}", module with same name already exist!`);
                continue;
            }

            let moduleInstance: Module;
            try {
                moduleInstance = (new (module as any)()) as Module;
            } catch (e) {
                logger.err(`Can't load module "${moduleInformation.name}" from "${moduleObjName}", module constructor failed!`, e);
                continue;
            }

            moduleInstance.information = moduleInformation;

            let allowLoad;
            try {
                allowLoad = await moduleInstance.allowLoad();
            } catch (e) {
                logger.err(`Can't check the allow to load the module "${moduleInformation.name}" from "${moduleObjName}"!`, e);
                continue;
            }

            if (!allowLoad) {
                logger.warn(`The module "${moduleInformation.name}" from "${moduleObjName}" was skipped, because it disallow loading!`);
                continue;
            }

            modules.set(moduleName, moduleInstance);
        }

        const countModulesForRegister = modules.size;
        let countRegisteredModules = 0;
        let everythingRegistered = true;

        const badModules: string[] = [];
        const unresolvedDependencyModules: {} = {};

        while (countRegisteredModules < countModulesForRegister && everythingRegistered) {
            everythingRegistered = false;

            for (const moduleName of modules.keys()) {
                if (badModules.indexOf(moduleName) > -1) {
                    continue;
                }

                const module: Module = modules.get(moduleName);

                try {
                    await this.registerModule(module);
                } catch (e) {
                    if (e instanceof UnresolvedDependencyError) {
                        unresolvedDependencyModules[moduleName] = module.information.name;
                        continue;
                    }

                    badModules.push(moduleName);
                    if (e instanceof ModuleInitializationError) {
                        logger.err(e.message, e.error());
                    } else if (e instanceof AlreadyRegisteredError) {
                        continue;
                    } else {
                        logger.err(e.message);
                        continue;
                    }
                }

                if (moduleName in unresolvedDependencyModules) {
                    delete unresolvedDependencyModules[moduleName];
                }

                ++countRegisteredModules;
                everythingRegistered = true;
            }
        }

        if (!everythingRegistered) {
            logger.warn('A recursive dependency of the modules was found, or one of the dependencies cannot be satisfied! ' +
                `Modules: ${Object.values(unresolvedDependencyModules).join(', ')}`);
        }

        const loadedModules = Array.from(this.modules.keys());
        requiredModules.forEach(moduleInfo => {
            if (loadedModules.indexOf(moduleInfo[0]) < 0) {
                logger.err(`A required module "${moduleInfo[1]}" from "${moduleInfo[2]}" cannot be loaded!`);
                throw new Error(`A required module "${moduleInfo[1]}" from "${moduleInfo[2]}" cannot be loaded!`);
            }
        });

        if (!countRegisteredModules) {
            logger.error('Not found any modules!');
            throw new Error('Not found any modules!');
        } else {
            logger.info(`Loaded ${countRegisteredModules} modules!`);
        }
    }

    public async registerModule(module: Module): Promise<void> {
        const moduleOriginalName = module.information.name;
        const moduleName = moduleOriginalName.toLowerCase();

        if (this.modules.has(moduleName)) {
            throw new AlreadyRegisteredError(moduleOriginalName);
        }

        if (module.information.dependencies && module.information.dependencies.length) {
            if (!module.information.dependencies.every(dependency => !!this.module(dependency))) {
                throw new UnresolvedDependencyError();
            }
        }

        logger.info(`Registration module "${moduleOriginalName}"...`);

        (module as any).logger = new Logger(module.information.loggerName ? module.information.loggerName : module.information.name);

        /** Do module initialization */
        try {
            await module.init();
        } catch (e) {
            throw new ModuleInitializationError(moduleOriginalName, e);
        }

        if (
            module.information.consoleCommands &&
            typeof module.information.consoleCommands === 'object' &&
            Object.keys(module.information.consoleCommands).length
        ) {
            Object.entries(module.information.consoleCommands).forEach(consoleCommand => {
                const handlers = this.modulesCommands.has(consoleCommand[0]) ? this.modulesCommands.get(consoleCommand[0]) : [];
                handlers.push(module);
                this.modulesCommands.set(consoleCommand[0], handlers);
            });
        }

        this.modules.set(moduleName, module);

        logger.info(`Module "${moduleOriginalName}" registered.`);
    }

    /**
     * Checking for the existence of a module by its name
     */
    public hasModule(name: string): boolean {
        name = name.trim().toLowerCase();
        return this.modules.has(name);
    }

    /**
     * Get a module by its name
     */
    public module(name: string): Module | null {
        name = name.trim().toLowerCase();
        return this.modules.has(name) ? this.modules.get(name) : null;
    }

    public requestState(moduleName: string, key: string, def: any = undefined): any {
        const module = this.module(moduleName);
        if (module) {
            return module.requestSelfState(key, def);
        }
        throw new Error('Module not found');
    }

    public async handleConsoleCommand(command: string, ...args: string[]): Promise<boolean> {
        if (this.modulesCommands.has(command)) {
            for (const module of this.modulesCommands.get(command)) {
                if (await module.handleConsoleCommand(command, ...args)) {
                    return true;
                }
            }
        }
        return false;
    }

    public async doRun(): Promise<void> {
        await Promise.all(Array.from(this.modules.values()).map(module => module.run()));
    }

    /**
     * Get all loaded modules names
     */
    public getModulesNames(): string[] {
        return Array.from(this.modules.values()).map(module => module.information.name);
    }

    /**
     * Get all available commands scoped by modules
     */
    public getConsoleCommands(): AvailableCommands {
        const commands: AvailableCommands = {};
        Array.from(this.modules.values()).forEach(module => {
            commands[module.information.name] = module.information.consoleCommands || {};
        });
        return commands;
    }
}
