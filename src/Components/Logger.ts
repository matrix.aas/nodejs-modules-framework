import {Writable} from "stream";

import moment from 'moment';
import colors from 'colors/safe';

const defaultOutputStreams: Array<Writable> = [
    process.stdout,
];
const allLoggers: Map<string, Logger> = new Map();

const loggerCreated = (logger: Logger) => {
    allLoggers.set(logger.channelName().toLowerCase(), logger);
}

export function addLoggerDefaultOutputStream(stream: Writable) {
    defaultOutputStreams.push(stream);
    for (const logger of allLoggers.values()) {
        logger.addOutputStream(stream);
    }
}

export function getLogger(channel: string): Logger | null {
    channel = channel.toLowerCase();
    return allLoggers.has(channel) ? allLoggers.get(channel) : null;
}

export default class Logger {
    protected channel: string;
    protected colors: any;
    protected outputStreams: Writable[] = [];
    protected maxLevelDeep = 12;
    protected enabled = true;

    constructor(channel: string) {
        this.channel = channel.toUpperCase();
        this.colors = colors as any;

        for (const stream of defaultOutputStreams) {
            this.addOutputStream(stream);
        }

        loggerCreated(this);
    }

    public addOutputStream(stream: Writable): this {
        this.outputStreams.push(stream);
        return this;
    }

    public info(message, ...args): void {
        this.prefix();
        this.write(`${this.prefixMessage('INFO', 'green')} ${this.colors.brightWhite(this.convertData(message))}`);
        this.printArgs(args);
        this.write('\n');
        this.postfix();
    }

    public debug(message, ...args): void {
        this.info(message, ...args);
    }

    public warn(message, ...args): void {
        this.prefix();
        this.write(`${this.prefixMessage('WARN', 'yellow')} ${this.colors.brightWhite(this.convertData(message))}`);
        this.printArgs(args);
        this.write('\n');
        this.postfix();
    }

    public err(message, ...args): void {
        this.prefix();
        this.write(`${this.prefixMessage('ERROR', 'red')} ${this.colors.brightWhite(this.convertData(message))}`);
        this.printArgs(args);
        this.write('\n');
        this.postfix();
    }

    public error(message, ...args): void {
        this.err(message, ...args);
    }

    public plain(message, ...args): void {
        this.write(message);
        this.printArgs(args);
        this.write('\n');
    }

    public simple(message: string, color: string = 'brightWhite'): void {
        this.prefix();
        this.write(`${this.time()} ${this.colors[color](this.convertData(message))}\n`);
    }

    public write(message, terminal = false): void {
        if (this.enabled) {
            this.colors.enable();

            this.outputStreams.forEach(stream => {
                if (stream !== process.stdout) {
                    message = message.replace(/\x1B.*?m/ig, '');
                }

                if (!terminal || stream === process.stdout) {
                    stream.write(message);
                }

                stream.emit('drain');
            });
        }
    }

    public replaceLine(): void {
        this.write('\r', true);
    }

    protected time(): string {
        return this.colors.gray(moment().format('D.M HH:mm:ss.SSS'));
    }

    protected prefix(): void {
        this.replaceLine();
    }

    protected prefixMessage(type, color) {
        return `${this.time()} ${this.colors[color](type)} ${this.colors.white(`[${this.channel}]`)}`;
    }

    protected postfix() {
        this.write('> ', true);
    }

    protected convertData(data) {
        if (data instanceof Error) {
            data = data.stack;
        } else if (typeof data === 'object') {
            data = this.stringifyColor(data, null, 2, 0);
        }
        return data;
    }

    protected printArgs(args) {
        if (args && Array.isArray(args) && args.length) {
            this.write(' ');
            args.forEach(arg => {
                this.write(`${this.convertData(arg)} `);
            });
        }
    }

    public channelName(): string {
        return this.channel;
    }

    public setEnabled(enabled: boolean): void {
        this.enabled = enabled;
    }

    public enable(): void {
        this.setEnabled(true);
    }

    public disable(): void {
        this.setEnabled(false);
    }

    private stringifyColor(obj: any, filter: Function | Object, indent = 0, level = 0): string {
        indent = indent || 0;
        level = level || 0;

        if (level > this.maxLevelDeep) {
            return `[MAX LEVEL ${this.maxLevelDeep}]`;
        }

        let output = '';
        if (typeof obj === 'string') {
            output += this.colors.grey('"') + this.colors.green(obj) + this.colors.grey('"');
        } else if (typeof obj === 'number') {
            output += this.colors.cyan('' + obj);
        } else if (typeof obj === 'boolean') {
            output += this.colors.red(obj ? 'true' : 'false');
        } else if (obj === null) {
            output += this.colors.blue('null');
        } else if (obj !== undefined && typeof obj !== 'function') {
            if (!Array.isArray(obj)) {
                output += this.colors.grey('{\n');
                Object.keys(obj).forEach(key => {
                    let value = obj[key];

                    if (filter) {
                        if (typeof filter === 'function') {
                            value = filter(key, value);
                        } else if (Array.isArray(filter)) {
                            if (filter.indexOf(key) < 0) {
                                return;
                            }
                        }
                    }

                    if (value === undefined) {
                        return;
                    }

                    output += ' '.repeat(indent + level * indent) + this.colors.grey('"') + this.colors.magenta(key) + this.colors.grey('":') + (indent ? ' ' : '');
                    output += this.stringifyColor(value, filter, indent, level + 1) + ',\n';
                });

                output = output.replace(/,\n$/, '\n');
                output += this.colors.grey(' '.repeat(level * indent) + '}');
            } else {
                output += this.colors.grey('[\n');
                obj.forEach(subObj => {
                    output += ' '.repeat(indent + level * indent) + this.stringifyColor(subObj, filter, indent, level + 1) + ',\n';
                });

                output = output.replace(/,\n$/, '\n');
                output += this.colors.grey(' '.repeat(level * indent) + ']');
            }
        }

        output = output.replace(/,$/gm, this.colors.grey(','));
        if (indent === 0) {
            return output.replace(/\n/g, '');
        }

        return output;
    }
}
