const webpack = require('webpack');
const path = require('path');
const JavaScriptObfuscator = require('webpack-obfuscator');
const childProcess = require('child_process');
const moment = require('moment');
const packageJson = require('./package.json');

const isProduction = process.env.NODE_ENV === 'production';

module.exports = {
    mode: isProduction ? 'production' : 'development',
    target: 'node',
    devtool: false,
    output: {
        filename: isProduction ? 'app.prod.js' : 'app.dev.js',
        path: path.resolve(__dirname, 'dist'),
        libraryTarget: 'commonjs2',
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
                options: {
                    appendTsSuffixTo: [
                        /\.vue$/
                    ]
                }
            },
            {
                test: /\.html$/i,
                loader: 'html-loader',
                options: {
                    minimize: false,
                    attributes: false,
                },
            },
        ],
    },
    plugins: (() => {
        const plugins = [
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify(process.env.NODE_ENV.trim()),
                    __VERSION__: JSON.stringify(packageJson.version),
                    __BUILD_NUMBER__: JSON.stringify(childProcess.execSync('git rev-list HEAD --count').toString().trim()),
                    __BUILD_DATE__: JSON.stringify(moment().format('DD.MM.YYYY HH:mm:ss').trim()),
                    FLUENTFFMPEG_COV: false,
                }
            }),
        ];
        if (isProduction) {
            plugins.push(new JavaScriptObfuscator({
                target: 'node',
                compact: true,
                simplify: true,
                disableConsoleOutput: true,
                identifierNamesGenerator: 'hexadecimal',
                renameGlobals: true,
                transformObjectKeys: false,
                stringArray: false,
                unicodeEscapeSequence: true,
                numbersToExpressions: true,
                reservedNames: [],
                reservedStrings: [],
            }));
        }
        return plugins;
    })(),
    resolve: {
        extensions: [
            '.js',
            '.ts',
        ],
    },
    entry: [
        path.resolve(__dirname, 'src/main.ts'),
    ],
    externals: []
};
